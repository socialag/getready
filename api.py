import json
import urllib
from concurrent.futures import ThreadPoolExecutor

import pandas
from tornado import web, ioloop, gen
from tornado.concurrent import run_on_executor

import status

thread_pool = ThreadPoolExecutor()


def prepare_data_frame():
    data_frame = pandas.read_csv("assets/dogs_of_nyc.csv")
    lowercase_data_frame = data_frame.applymap(lambda s: s.lower() if type(s) == str else s)
    return lowercase_data_frame


def get_valid_column_name(data_frame):
    return list(data_frame.columns)


class CountHandler(web.RequestHandler):
    SUPPORTED_METHODS = ("GET", )
    _thread_pool = thread_pool

    store = {}
    lowercase_data_frame = prepare_data_frame()
    valid_column_names = get_valid_column_name(lowercase_data_frame)

    @gen.coroutine
    def get(self):
        query_parameters = dict(urllib.parse.parse_qsl(self.request.query.lower()))
        is_valid_request, unknown_fields = self.validate_parameters(query_parameters.keys())
        if not is_valid_request:
            self.set_status(status_code=status.HTTP_400_BAD_REQUEST)
            response = {
                'unknown fields': unknown_fields
            }
        else:
            count = yield self.get_count(query_parameters)
            response = {
                'count': count,
            }

        self.write(json.dumps(response))
        self.finish()

    def set_default_headers(self, *args, **kwargs):
        self.set_header("Access-Control-Allow-Methods", "GET")
        self.set_header("Content-Type", "application/json")

    def get_request_parameters(self):
        return self.request.arguments.keys()

    def validate_parameters(self, parameters):
        is_valid_request = True
        unknown_fields = []
        for parameter in parameters:
            if parameter not in self.valid_column_names:
                unknown_fields.append(parameter)
            if not parameters or unknown_fields:
                unknown_fields.sort()
                is_valid_request = False
        return is_valid_request, unknown_fields

    @run_on_executor(executor="_thread_pool")
    def get_count(self, parameters):
        store_key = self.request.query
        if store_key in self.store:
            count = self.store[store_key]
        else:
            data_frame = self.lowercase_data_frame
            for key, value in parameters.items():
                data_frame = data_frame[(data_frame[key] == value)]
            count = data_frame.shape[0]
            self.store[store_key] = count
            self.set_status(status_code=status.HTTP_200_OK)
        return count


application = web.Application(
    [
        (r"/count", CountHandler),
    ], debug=False
)

if __name__ == "__main__":
    prepare_data_frame()
    port = 5000
    print("Listening at port {0}".format(port))
    application.listen(port)
    ioloop.IOLoop.instance().start()
