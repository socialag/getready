# README #

This project is to present a solution to the Ready Game backend engineer candidates test.
[Ready Game test](https://gist.github.com/aparrish/691b0301f6737d65b01db9920a60a0a5)

### Hosted service ###
The application is currently hosted on amazon cloudfront. A docker container run as a service.

You can run the test against this web service by running the following command:

    READY_TEST_BASE_URL=http://getready.soma.ai python apitest.py

The service url is:

[http://getready.soma.ai/count](http://getready.soma.ai/count)

You can pass more parameters.

For example:

[http://getready.soma.ai/count?dominant_color=brown](http://getready.soma.ai/count?dominant_color=brown)

[http://getready.soma.ai/count?fleas=no](http://getready.soma.ai/count?fleas=no)

### Local installation ###

    * Create your python virtual environment
    * pip install -r requirements.txt
    * python api.py
    You should see “Listening at port 5000” prompt
see (How to tests locally) below

### Docker installation ###

    * docker build -t getready . --network=host
    * docker run --detach=false --publish=5000:5000 getready:latest
see (How to tests locally) below

### How to tests locally ###

You can now run the test using bash by typing:

    READY_TEST_BASE_URL=http://localhost:5000 python apitest.py

The local service url is:

[http://localhost:5000/count](http://localhost:5000/count)

You can pass more parameters.

For example:

[http://localhost:5000/count?dominant_color=brown](http://localhost:5000/count?dominant_color=brown)

[http://localhost:5000/count?fleas=no](http://localhost:5000/count?fleas=no)
