FROM python:3.7-alpine
WORKDIR /app
RUN apk update && apk upgrade
RUN apk add --no-cache libpng freetype libstdc++ python py-pip
RUN apk add --no-cache --virtual .build-deps \
            gcc \
            build-base \
            python-dev \
            libpng-dev \
            musl-dev \
            freetype-dev

# By copying over requirements first, we make sure that Docker will cache
# our installed requirements rather than reinstall them on every build
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

# Now copy in our code, and run it
COPY . /app

EXPOSE  5000
CMD ["python", "/app/api.py", "-p 5000"]
